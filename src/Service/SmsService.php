<?php
/**
 * Created by PhpStorm.
 * User: Aristide BATIONO
 * Date: 27/10/2018
 * Time: 17:00
 */
namespace App\Service;

class SmsService
{
    public function smsSend($tel,  $msg , $sender)
    {
        $num = "";
        $elem = explode(" ", $tel);
        foreach($elem as $val){
            if($val != ""){
                $num .=	rtrim($val);
            }

        }

        $tel  = ltrim($num, '+');

        $str = 'rdv:c*_qYv5k5';
        $base =  base64_encode($str);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://api.infobip.com/sms/1/text/single",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode(array( 'from' => $sender, 'to' => $tel, 'text' => $msg )) ,
                CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "authorization: Basic $base",
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

    }
    function urlencode_array($array_args) {
        if (! is_array($array_args) )
            return false;
        $pairs=array();
        foreach ($array_args as $k=>$v) {
            $pairs[] = "$k=" . rawurlencode($v);
        }
        return implode($pairs, "&");
    }

    public function smsSendCodeRdv($tel, $code , $date)
    {
        $dateFormat = $date->format('d-m-Y H:i');
        $cod = $code->getId();
       $msg = "Pour Valider votre rdv du $dateFormat , veuillez entrer le code $cod sur le site.";
       $this->smsSend($tel,  $msg , 'RDVMEDECINE');
    }

}