<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @UniqueEntity("mail")
 * @ORM\HasLifecycleCallbacks()
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rdv", mappedBy="client" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $rdv;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Code", mappedBy="client" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephone;

    /**
     *
     * @ORM\Column(type="string", length=255 , nullable=true)
     *  @Assert\Email(
     *     message = "l'adresse '{{ value }}' est invalide.",
     *     checkMX = true
     * )
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $localisation;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $naissanceDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $naissanceLieu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Localisation")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Localisation")
     * @ORM\JoinColumn(nullable=true)
     */
    private $commune;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;

    public function __construct()
    {
        $this->rdv = new ArrayCollection();
        $this->code = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }


    public function getIdentifiant()
    {
        return $this->encrypt('encrypt',$this->id);
    }
    function encrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'ffgd;jhfserfy@!$DCGGgssd';
        $secret_iv = 'ffgd;jhfserfy@@!$DCGGgssd';
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if($this->id != null){
            $this->modified = new \DateTime();
        }else {
            $this->created = new \DateTime();
            $this->modified = new \DateTime();
        }

    }

    public function getNaissanceLieu(): ?string
    {
        return $this->naissanceLieu;
    }

    public function setNaissanceLieu(string $naissanceLieu): self
    {
        $this->naissanceLieu = $naissanceLieu;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getVille(): ?Localisation
    {
        return $this->ville;
    }

    public function setVille(?Localisation $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCommune(): ?Localisation
    {
        return $this->commune;
    }

    public function setCommune(?Localisation $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getNaissanceDate(): ?\DateTimeInterface
    {
        return $this->naissanceDate;
    }

    public function setNaissanceDate(\DateTimeInterface $naissanceDate): self
    {
        $this->naissanceDate = $naissanceDate;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * @return Collection|Rdv[]
     */
    public function getRdv(): Collection
    {
        return $this->rdv;
    }

    public function addRdv(Rdv $rdv): self
    {
        if (!$this->rdv->contains($rdv)) {
            $this->rdv[] = $rdv;
            $rdv->setClient($this);
        }

        return $this;
    }

    public function removeRdv(Rdv $rdv): self
    {
        if ($this->rdv->contains($rdv)) {
            $this->rdv->removeElement($rdv);
            // set the owning side to null (unless already changed)
            if ($rdv->getClient() === $this) {
                $rdv->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Code[]
     */
    public function getCode(): Collection
    {
        return $this->code;
    }

    public function addCode(Code $code): self
    {
        if (!$this->code->contains($code)) {
            $this->code[] = $code;
            $code->setClient($this);
        }

        return $this;
    }

    public function removeCode(Code $code): self
    {
        if ($this->code->contains($code)) {
            $this->code->removeElement($code);
            // set the owning side to null (unless already changed)
            if ($code->getClient() === $this) {
                $code->setClient(null);
            }
        }

        return $this;
    }
}
