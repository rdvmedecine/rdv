<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrestataireRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Prestataire implements ObjectManagerAware
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var text
     *
     * @ORM\Column( type="text" , nullable=true)
     */
    private $presentation;

    private $em;

    public function injectObjectManager(ObjectManager $objectManager,ClassMetadata $classMetadata)
    {
        $this->em = $objectManager;
    }

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Assert\Valid
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tarif", mappedBy="prestataire" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $tarif;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Agenda", mappedBy="prestataire" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $agenda;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rdv", mappedBy="prestataire" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $rdv;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Localisation")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Localisation")
     * @ORM\JoinColumn(nullable=true)
     */
    private $commune;

    /**
     * @var text
     *
     * @ORM\Column(type="text" , nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Prestataire", mappedBy="parent" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $childs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestataire", inversedBy="childs")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $lienFace;

    /**
     * @var string
     *
     * @ORM\Column( type="string", length=255 , nullable=true)
     */
    private $lienTwiter;

    /**
     * @var string
     *
     * @ORM\Column( type="string", length=255 , nullable=true)
     */
    private $lienLinkedin;

    /**
     * @var string
     *
     * @ORM\Column( type="string", length=255 , nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $latitude;

    /**
     * @var text
     *
     * @ORM\Column(type="text" , nullable=true)
     */
    private $autreInfo;

    /**
     * @var text
     *
     * @ORM\Column( type="text" , nullable=true)
     */
    private $moyenTransport;

    /**
     * @var text
     *
     * @ORM\Column(type="text" , nullable=true)
     */
    private $decrispTarif;

    /**
     * @var text
     *
     * @ORM\Column( type="text" , nullable=true)
     */
    private $commentaire;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $langue;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean" , nullable=true)
     */
    private $etat;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Entreprise", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $entreprise;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Particulier", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $particulier;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;

    public function __construct()
    {
        $this->tarif = new ArrayCollection();
        $this->agenda = new ArrayCollection();
        $this->childs = new ArrayCollection();
        $this->rdv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData()
    {
           if($this->getEntreprise()){
               return $this->getEntreprise();
           }else{
               return $this->getParticulier();
           }
    } 
    public function getNom()
    {
        if($this->getEntreprise()){
            return $this->getEntreprise()->getNom();
        }else{
            return $this->getParticulier()->getNom().' '.$this->getParticulier()->getPrenom();
        }
    }

    public function getLocalisation()
    {
        if($this->getEntreprise()){
            return $this->getEntreprise()->getLocalisation();
        }else{
            return $this->getParticulier()->getLocalisation();
        }
    } 

    public function getTelephone()
    {
        if($this->getEntreprise()){
            return $this->getEntreprise()->getTelephoneContact();
        }else{
            return $this->getParticulier()->getNom().' '.$this->getParticulier()->getTelephone();
        }
    }

    public function getType()
    {
        if($this->getEntreprise()){
            return 'entreprise';
        }else{
            return 'particulier';
        }
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getIdentifiant()
    {
        return $this->encrypt('encrypt',$this->id);
    }
    function encrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'ffgd;jhfserfy@!$DCGGgssd';
        $secret_iv = 'ffgd;jhfserfy@@!$DCGGgssd';
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if($this->id != null){
            $this->modified = new \DateTime();
        }else {
            $this->created = new \DateTime();
            $this->modified = new \DateTime();
        }

    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLienFace(): ?string
    {
        return $this->lienFace;
    }

    public function setLienFace(?string $lienFace): self
    {
        $this->lienFace = $lienFace;

        return $this;
    }

    public function getLienTwiter(): ?string
    {
        return $this->lienTwiter;
    }

    public function setLienTwiter(?string $lienTwiter): self
    {
        $this->lienTwiter = $lienTwiter;

        return $this;
    }

    public function getLienLinkedin(): ?string
    {
        return $this->lienLinkedin;
    }

    public function setLienLinkedin(?string $lienLinkedin): self
    {
        $this->lienLinkedin = $lienLinkedin;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getAutreInfo(): ?string
    {
        return $this->autreInfo;
    }

    public function setAutreInfo(?string $autreInfo): self
    {
        $this->autreInfo = $autreInfo;

        return $this;
    }

    public function getMoyenTransport(): ?string
    {
        return $this->moyenTransport;
    }

    public function setMoyenTransport(?string $moyenTransport): self
    {
        $this->moyenTransport = $moyenTransport;

        return $this;
    }

    public function getDecrispTarif(): ?string
    {
        return $this->decrispTarif;
    }

    public function setDecrispTarif(?string $decrispTarif): self
    {
        $this->decrispTarif = $decrispTarif;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getLangue(): ?string
    {
        return $this->langue;
    }

    public function setLangue(?string $langue): self
    {
        $this->langue = $langue;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(?bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getParticulier(): ?Particulier
    {
        return $this->particulier;
    }

    public function setParticulier(?Particulier $particulier): self
    {
        $this->particulier = $particulier;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Tarif[]
     */
    public function getTarif(): Collection
    {
        return $this->tarif;
    }

    public function addTarif(Tarif $tarif): self
    {
        if (!$this->tarif->contains($tarif)) {
            $this->tarif[] = $tarif;
            $tarif->setPrestataire($this);
        }

        return $this;
    }

    public function removeTarif(Tarif $tarif): self
    {
        if ($this->tarif->contains($tarif)) {
            $this->tarif->removeElement($tarif);
            // set the owning side to null (unless already changed)
            if ($tarif->getPrestataire() === $this) {
                $tarif->setPrestataire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Agenda[]
     */
    public function getAgenda(): Collection
    {
        return $this->agenda;
    }

    public function addAgenda(Agenda $agenda): self
    {
        if (!$this->agenda->contains($agenda)) {
            $this->agenda[] = $agenda;
            $agenda->setPrestataire($this);
        }

        return $this;
    }

    public function removeAgenda(Agenda $agenda): self
    {
        if ($this->agenda->contains($agenda)) {
            $this->agenda->removeElement($agenda);
            // set the owning side to null (unless already changed)
            if ($agenda->getPrestataire() === $this) {
                $agenda->setPrestataire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Prestataire[]
     */
    public function getChilds(): Collection
    {
        return $this->childs;
    }

    public function addChild(Prestataire $child): self
    {
        if (!$this->childs->contains($child)) {
            $this->childs[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Prestataire $child): self
    {
        if ($this->childs->contains($child)) {
            $this->childs->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getVille(): ?Localisation
    {
        return $this->ville;
    }

    public function setVille(?Localisation $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCommune(): ?Localisation
    {
        return $this->commune;
    }

    public function setCommune(?Localisation $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * @return Collection|Rdv[]
     */
    public function getRdv(): Collection
    {
        return $this->rdv;
    }

    public function addRdv(Rdv $rdv): self
    {
        if (!$this->rdv->contains($rdv)) {
            $this->rdv[] = $rdv;
            $rdv->setPrestataire($this);
        }

        return $this;
    }

    public function removeRdv(Rdv $rdv): self
    {
        if ($this->rdv->contains($rdv)) {
            $this->rdv->removeElement($rdv);
            // set the owning side to null (unless already changed)
            if ($rdv->getPrestataire() === $this) {
                $rdv->setPrestataire(null);
            }
        }

        return $this;
    }

    public function getAgendas($semaine)
    {
        $time =  array();
        $periode =  array();
        $nb = 0;
        $retour = [];
        $debut = new \DateTime($semaine);

        $fin = new \DateTime($semaine);

        foreach($this->getAgenda() as $disponible){


            $debut->modify($disponible->getJourAng().' '. $disponible->getHeureD()->format('H:i'));

            $fin->modify($disponible->getJourAng().' '. $disponible->getHeureF()->format('H:i'));



            while($debut <= $fin) {

                $tmp = new \DateTime($debut->format('Y-m-d H:i'));

                $now =  new \DateTime();

                if($tmp > $now ){

                    if (!in_array($tmp, $time)){
                        array_push($time , $tmp );
                        $item = [];
                        $item['heure'] =  $tmp->format('H:i');
                        $item['date'] =   $tmp->format('Y-m-d');
                        $item['active'] =   false;
                        $item['periode'] = $tmp->format('d').' '.$this->getFrench($tmp->format('F'));
                        $item['jour'] =  $disponible->getJourFrench();
                        $periode[$disponible->getJourFrench()][] =  $item;

                    }
                }
                $debut->modify('+'.$disponible->getCreno().' minutes');
            }

            if (array_key_exists($disponible->getJourFrench(), $periode)) {
                $i = count($periode[$disponible->getJourFrench()]);
                if($i >= $nb ) $nb = $i;
            }



        }




        $retour['nb'] = $nb;
        $retour['periode'] = $periode;
        return $retour;
    }

    


    public function getHeure($semaine)
    {
        $time =  array();
        $periode =  array();
        $nb = 0;
        $retour = [];
        $debut = new \DateTime($semaine);
        $fin = new \DateTime($semaine);

        foreach($this->getAgenda() as $disponible){

            $debut->modify($disponible->getJourAng().' '. $disponible->getHeureD()->format('H:i'));

            $fin->modify($disponible->getJourAng().' '. $disponible->getHeureF()->format('H:i'));

            while($debut <= $fin) {

                $tmp = new \DateTime($debut->format('Y-m-d H:i'));
                $now =  new \DateTime();

                if($tmp > $now ){

                    if (!in_array($tmp, $time)){
                        array_push($time , $tmp );
                        $item = [];
                        $item['heure'] =  $tmp->format('H:i');
                        $item['date'] =   $tmp->format('Y-m-d');
                        $item['active'] =   false;
                        $item['periode'] = $tmp->format('d').' '.$this->getFrench($tmp->format('F'));
                        $item['jour'] =  $disponible->getJourFrench();
                        $periode[] =  $item;

                    }
                }
                $debut->modify('+'.$disponible->getCreno().' minutes');
            }

            if (array_key_exists($disponible->getJourFrench(), $periode)) {
                $i = count($periode[$disponible->getJourFrench()]);
                if($i >= $nb ) $nb = $i;
            }
        }

        return $periode;
    }


    public function getFrench($date)
    {
        $liste = array(
            'january' => 'janvier',
            'february' => 'février',
            'march' => 'mars',
            'april' => 'avril',
            'may' => 'mai',
            'june' => 'juin',
            'july' => 'juillet',
            'august' => 'août',
            'september' => 'septembre',
            'october' => 'octobre',
            'november' => 'novembre',
            'december' => 'décembre',
        );

        return $liste[strtolower($date)];

    }
    protected function getUploadRootDir()
    {
        return __DIR__.'/../public/'.$this->getUploadDir();
    }
    public function getUploadDir()
    {
        return 'uploads';
    }

    public function getWebPath()
    {
        if( $this->getImage() != null  ){
            return $this->getImage()->getWebPath();
        }else{
            return $this->getUploadDir().'/avatar.png';
        }
    
    }
}
