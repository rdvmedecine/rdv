<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgendaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Agenda
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jourFrench;

    /**
     * @ORM\Column(type="integer")
     */
    private $creno;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestataire", inversedBy="agenda")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=false)
     */
    private $prestataire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="time")
     */
    private $heureD;

    /**
     * @var \DateTime
     *
     * @ORM\Column( type="time")
     */
    private $heureF;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJourFrench(): ?string
    {
        return $this->jourFrench;
    }

    public function setJourFrench(string $jourFrench): self
    {
        $this->jourFrench = $jourFrench;

        return $this;
    }

    public function getCreno(): ?int
    {
        return $this->creno;
    }

    public function setCreno(int $creno): self
    {
        $this->creno = $creno;

        return $this;
    }

    function encrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'ffgd;jhfserfy@!$DCGGgssd';
        $secret_iv = 'ffgd;jhfserfy@@!$DCGGgssd';
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if($this->id != null){
            $this->modified = new \DateTime();
        }else {
            $this->created = new \DateTime();
            $this->modified = new \DateTime();
        }

    }

    public function getHeureD(): ?\DateTimeInterface
    {
        return $this->heureD;
    }

    public function setHeureD(\DateTimeInterface $heureD): self
    {
        $this->heureD = $heureD;

        return $this;
    }

    public function getHeureF(): ?\DateTimeInterface
    {
        return $this->heureF;
    }

    public function setHeureF(\DateTimeInterface $heureF): self
    {
        $this->heureF = $heureF;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getPrestataire(): ?Prestataire
    {
        return $this->prestataire;
    }

    public function setPrestataire(?Prestataire $prestataire): self
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     *
     *
     * @return string
     */
    public function getJourAng()
    {
        $liste = array(
            'Lundi' => 'Monday',
            'Mardi' => 'Tuesday',
            'Mercredi' => 'Wednesday',
            'Jeudi' => 'Thursday',
            'Vendredi' => 'Friday',
            'Samedi' => 'Saturday',
            'Dimanche' => 'Sunday',
        );

        return $liste[$this->jourFrench];
    }
}
