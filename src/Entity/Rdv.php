<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RdvRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Rdv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="rdv")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prestataire", inversedBy="rdv")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $prestataire;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaireAvant;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaireApres;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Code", mappedBy="rdv" , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $code;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Image", cascade={"persist"} , cascade={"persist","remove"} , orphanRemoval=true)
     */
    private $images;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->code = new ArrayCollection();
    }

    public function getIdentifiant()
    {
        return $this->encrypt('encrypt',$this->id);
    }
    function encrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'ffgd;jhfserfy@!$DCGGgssd';
        $secret_iv = 'ffgd;jhfserfy@@!$DCGGgssd';
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if($this->id != null){
            $this->modified = new \DateTime();
        }else {
            $this->created = new \DateTime();
            $this->modified = new \DateTime();
        }

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCommentaireAvant(): ?string
    {
        return $this->commentaireAvant;
    }

    public function setCommentaireAvant(?string $commentaireAvant): self
    {
        $this->commentaireAvant = $commentaireAvant;

        return $this;
    }

    public function getCommentaireApres(): ?string
    {
        return $this->commentaireApres;
    }

    public function setCommentaireApres(?string $commentaireApres): self
    {
        $this->commentaireApres = $commentaireApres;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getPrestataire(): ?Prestataire
    {
        return $this->prestataire;
    }

    public function setPrestataire(?Prestataire $prestataire): self
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    /**
     * @return Collection|Code[]
     */
    public function getCode(): Collection
    {
        return $this->code;
    }

    public function addCode(Code $code): self
    {
        if (!$this->code->contains($code)) {
            $this->code[] = $code;
            $code->setRdv($this);
        }

        return $this;
    }

    public function removeCode(Code $code): self
    {
        if ($this->code->contains($code)) {
            $this->code->removeElement($code);
            // set the owning side to null (unless already changed)
            if ($code->getRdv() === $this) {
                $code->setRdv(null);
            }
        }

        return $this;
    }
}
