<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Localisation;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/client")
 */
class ClientController extends Controller
{
    /**
     * @Route("/", name="client_index", methods="GET")
     */
    public function index(ClientRepository $clientRepository): Response
    {
        return $this->render('client/index.html.twig', ['clients' => $clientRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="client_new", methods="GET|POST")
     */
    public function new(Request $request, $id = null): Response
    {
        $client = new Client();
        $em = $this->getDoctrine()->getManager();

        if($id != null ){
            $client = $em->getRepository(Client::class)->find($id);
        }
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);
        $villes = $em->getRepository(Localisation::class)->findBy( ['type' => 'ville'] );

        if ($form->isSubmitted() && $form->isValid()) {
            if($request->get('ville')){
                $ville = $em->getRepository(Localisation::class)->find($request->get('ville'));
                if($ville) $client->setVille($ville);
            }
            if($request->get('commune')){
                $commune = $em->getRepository(Localisation::class)->find($request->get('commune'));
                if($commune) $client->setCommune($commune);
            }

            $em->persist($client);
            $em->flush();

            //return $this->redirectToRoute('client_index');
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('client_show' , [ 'id' => $client->getIdentifiant()  ] );
        }

        return $this->render('client/new.html.twig', [
            'client' => $client,
            'villes' => $villes,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="client_show", methods="GET")
     */
    public function show(Client $client): Response
    {
        return $this->render('client/show.html.twig', ['client' => $client]);
    }

    /**
     * @Route("/{id}/edit", name="client_edit", methods="GET|POST")
     */
    public function edit(Request $request, Client $client): Response
    {
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('client_edit', ['id' => $client->getId()]);
        }

        return $this->render('client/edit.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="client_delete", methods="DELETE")
     */
    public function delete(Request $request, Client $client): Response
    {
        if ($this->isCsrfTokenValid('delete'.$client->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
        }

        return $this->redirectToRoute('client_index');
    }
}
