<?php

namespace App\Controller;

use App\Entity\Vendeur;
use App\Form\VendeurType;
use App\Repository\VendeurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/vendeur")
 */
class VendeurController extends Controller
{
    /**
     * @Route("/", name="vendeur_index", methods="GET")
     */
    public function index(VendeurRepository $vendeurRepository): Response
    {
        return $this->render('vendeur/index.html.twig', ['vendeurs' => $vendeurRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="vendeur_new", methods="GET|POST")
     */
    public function new(Request $request, $id = null): Response
    {
        $vendeur = new Vendeur();
        $em = $this->getDoctrine()->getManager();
        if($id != null ){
            $vendeur = $em->getRepository(Vendeur::class)->find($id);
        }
       
        $form = $this->createForm(VendeurType::class, $vendeur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vendeur);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('vendeur_show' , [ 'id' => $vendeur->getIdentifiant()  ] );
        }

        return $this->render('vendeur/new.html.twig', [
            'vendeur' => $vendeur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vendeur_show", methods="GET")
     */
    public function show(Vendeur $vendeur): Response
    {
        return $this->render('vendeur/show.html.twig', ['vendeur' => $vendeur]);
    }

    /**
     * @Route("/{id}/edit", name="vendeur_edit", methods="GET|POST")
     */
    public function edit(Request $request, Vendeur $vendeur): Response
    {
        $form = $this->createForm(VendeurType::class, $vendeur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vendeur_edit', ['id' => $vendeur->getId()]);
        }

        return $this->render('vendeur/edit.html.twig', [
            'vendeur' => $vendeur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vendeur_delete", methods="DELETE")
     */
    public function delete(Request $request,  Vendeur $vendeur): Response
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->isCsrfTokenValid('delete'.$vendeur->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vendeur);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'L\'élément a bien été supprimé!'); 
        }

        return $this->redirectToRoute('vendeur_index');
    }

}
