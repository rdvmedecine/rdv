<?php

namespace App\Controller;

use App\Entity\Slide;
use App\Form\SlideType;
use App\Repository\SlideRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/slide")
 */
class SlideController extends Controller
{
    /**
     * @Route("/", name="slide_index", methods="GET")
     */
    public function index(SlideRepository $slideRepository): Response
    {
        return $this->render('slide/index.html.twig', ['slides' => $slideRepository->findAll()]);
    }

    /**
     * @Route("/new/{id}", name="slide_new", methods="GET|POST")
     */
    public function new(Request $request , $id = null ): Response
    {

        $slide = new Slide();
        $em = $this->getDoctrine()->getManager();
        if($id != null ){
            $slide = $em->getRepository(Slide::class)->find($id);
        }
    
        $form = $this->createForm(SlideType::class, $slide);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em->persist($slide);
            $em->flush();
            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('slide_show' , [ 'id' => $slide->getIdentifiant()  ] );
        }

        return $this->render('slide/new.html.twig', [
            'slide' => $slide,
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/{id}", name="slide_show", methods="GET")
     */
    public function show(Slide $slide): Response
    {
        return $this->render('slide/show.html.twig', ['slide' => $slide]);
    }


    /**
     * @Route("/{id}", name="slide_delete", methods="DELETE")
     */
    public function delete(Request $request, Slide $slide): Response
    {
        if ($this->isCsrfTokenValid('delete'.$slide->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($slide);
            $em->flush();
        }
        $request->getSession()->getFlashBag()->add('ajout', 'L\'élément a bien été supprimé!');
        return $this->redirectToRoute('slide_index');
    }
}
