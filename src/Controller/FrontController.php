<?php

namespace App\Controller;

use App\Entity\Rdv;
use App\Entity\User;
use App\Entity\Client;
use App\Form\ClientType;
use App\Entity\Prestataire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class FrontController extends Controller
{
    /**
     * @Route("/", name="front")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $slides = $em->getRepository("App:Slide")->findAll();
        return $this->render('front/index.html.twig', [
            'slides' => $slides,
        ]);
    }

    /**
     * @Route("/search/data", name="search_data")
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prestataires = $em->getRepository('App:Prestataire')->findAll( );
        $specialite = $em->getRepository('App:Category')->findAll( );
        $data = [];
        foreach ($prestataires as $key => $value) {
        		$tmp = [];
        		$tmp['value'] = $value->getNom();
        		$tmp['id'] = $value->getId();
        		$tmp['type'] = 'prestataire';

        		$data[] = $tmp;
        }
         foreach ($specialite as  $service) {
        		$tmp = [];
        		$tmp['value'] = $service->getLibelle();
        		$tmp['id'] = $service->getId();
        		$tmp['type'] = 'service';

        		$data[] = $tmp;
        }

         	echo json_encode($data);


      	die();

    }

    /**
     * @Route("/recherche/{type}/{adresse}", name="prestataires")
     */
    public function prestatairesAction(Request $request , $type, $adresse = null)
    {
        $em = $this->getDoctrine()->getManager();
        $prestataires = null;
        $type = str_replace("+"," ",$type);
        $adresse = str_replace("+"," ",$adresse);
        
        $prestataires = $em->getRepository('App:Prestataire')->findPrestataire($type,$adresse);
           return $this->render('front/prestaires.html.twig', array( 
 
                'prestataires' => $prestataires,
                'type' => $type,
                'adresse' => $adresse,

                ));


    }

    /**
     * @Route("/prestataire-infos/{name}/{id}", name="prestataires_info")
     */
    public function prestatairesInfosAction(Prestataire $prestataire)
    {
        $em = $this->getDoctrine()->getManager();
           return $this->render('front/prestataireInfos.html.twig', array( 
 
                'prestataire' => $prestataire,
                ));
    }

    /**
     * @Route("/prise-de-rendez-vous/{name}/{id}", name="prestataires_prise_rdv")
     */
    public function prestatairesPriseAction(Prestataire $prestataire)
    {
        $em = $this->getDoctrine()->getManager();
           return $this->render('front/validation-rendez-vous.html.twig', array( 
 
                'prestataire' => $prestataire,
                ));
    }

    /**
     * @Route("/prestataires-agendas/{id}/agenda", name="prestataires_agenda")
     */
    public function prestatairesAgendaAction(Prestataire $prestataire, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prestataires = null;

        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
            $date = new \DateTime($parametersAsArray['semaine']);
            $agendas =  $prestataire->getAgendas($date->format('Y-m-d'));
           echo json_encode($agendas);
        }
       	 

      	die();

    }
    /**
     * @Route("/connexion-client", name="client_connexion", methods="GET|POST")
     */
    public function connexionCient(Request $request , UserPasswordEncoderInterface $encoder , TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager): Response
    {
        if ($request->isMethod('POST')){
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
                //dump($this->getUser());
                $data = $parametersAsArray['data'];
                $etat = $this->login($data['mail'], $data['password'] , $encoder);
                if($etat){
                    $em = $this->getDoctrine()->getManager();
                    $user =  $em->getRepository('App:User')->findOneByEmail($data['mail']);
                    $token = new UsernamePasswordToken($user, $user->getPassword() , 'main', $user->getRoles());
                    $this->get('security.token_storage')->setToken($token);
                    $request->getSession()->set('_security_main', serialize($token));
                    echo json_encode(['code' => 1, 'msg' => 'Acces OK !' ]);
                }else{
                    echo json_encode(['code' => 2, 'msg' => 'Acces invalides !' ]);
                }
                die();
            }
        }

        die();
    }

    /**
     * @Route("/client-validations/{prest}", name="rdv_validations_client")
     */
    public function validations($prest , Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
            $date = new \DateTime($parametersAsArray['date']);
            $time = explode(':',$parametersAsArray['heure']);
            $date->setTime($time[0], $time[1]);
            $prestataire = $em->getRepository(Prestataire::class)->find($parametersAsArray['prestataire']);
            $client = $em->getRepository(Client::class)->find($parametersAsArray['client']);
            $rdv = $em->getRepository(Rdv::class)->findOneBy( [ 'date' =>  $date , 'prestataire' => $prestataire ] );
            $retour = [];
            $message = $this->container->get('app.sms');
            // $tel = $client->getVille()->getPays()->getIndicatif().$client->getTelephone();
            $tel = "+225".$client->getTelephone();
            $code = new \App\Entity\Code();
            $code->setEtat(false);
            $code->setClient($client);
            $em->persist($code);
            $em->flush();
            $message->smsSendCodeRdv($tel, $code , $date);
            if($rdv){
                $retour = array( 'code' => 0 , 'message' => 'Le Prestataire n\'est pas disponible a cette preiode' );
            }else{
                $retour = array( 'code' => 1 , 'message' => 'SMS Envoyer' , 'data' => $tel  );
            }
            echo json_encode($retour);
        }
        die();
    }

    /**
     * @Route("/client-confirmation/{prest}", name="rdv_confirmartion_client")
     */
    public function confirmation($prest , Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
            $date = new \DateTime($parametersAsArray['date']);
            $time = explode(':',$parametersAsArray['heure']);
            $date->setTime($time[0], $time[1]);
            $prestataire = $em->getRepository(Prestataire::class)->find($parametersAsArray['prestataire']);
            $client = $em->getRepository(Client::class)->find($parametersAsArray['client']);
            $codeRdv = $em->getRepository(\App\Entity\Code::class)->find($parametersAsArray['code']);
            $retour = [];

            $message = $this->container->get('app.sms');

            //$tel = $client->getVille()->getPays()->getIndicatif().$client->getTelephone();
            $tel = "+225".$client->getTelephone();
            if($codeRdv){
                $rdv = new Rdv();
                $rdv->setDate($date);
                $rdv->setClient($client);
                $rdv->setPrestataire($prestataire);
                $rdv->setCommentaireAvant($parametersAsArray['commentaire']);
                $rdv->setStatus(0);
                $rdv->addCode($codeRdv);
                $em->persist($rdv);
                $em->flush();
                $retour = array( 'code' => 1 , 'message' => 'RDV confirmer'  );
            }else{
                $retour = array( 'code' => 0 , 'message' => 'Le code est invalide' );
            }
            echo json_encode($retour);
        }
        die();
    }

    /**
     * @Route("/new-client", name="client_add", methods="GET|POST")
     */
    public function newCient(Request $request , UserPasswordEncoderInterface $encoder): Response
    {
        $client = new Client();
        $user = new User();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')){
            if ($content = $request->getContent()) {
                $parametersAsArray = json_decode($content, true);
                $data = $parametersAsArray['data'];
                $email = $em->getRepository('App:User')->findOneByEmail($data['mail']);
                $group = $em->getRepository('App:Groupes')->findOneByName('Client');
                if(!$email){
                    $client->setNom($data['nom']);
                    $client->setPrenom($data['prenom']);
                    $client->setTelephone($data['telephone']);
                    $client->setLocalisation($data['localisation']);
                    $client->setMail($data['mail']);
                    $user->setEmail($data['mail']);
                    $hash = $encoder->encodePassword($user, $data['password'] );
                    $user->setPassword($hash);
                    $user->setClient($client);
                    $user->addGroupe($group);
                    $em->persist($user);
                    $em->flush();
                    $token = new UsernamePasswordToken($user, $user->getPassword() , 'main', $user->getRoles());
                    $this->get('security.token_storage')->setToken($token);
                    $request->getSession()->set('_security_main', serialize($token));
                    echo json_encode(['code' => 1, 'msg' => 'Inscription Ok' ]);
                }else{
                    echo json_encode(['code' => 2, 'msg' => 'Le Mail existe deja !' ]);
                }
                die();
            }
        }
       die();
    }
    
    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {

        return $this->redirectToRoute('front');
    }

    /**
     * @Route("/login", name="user_login")
     */
    public function loginUser()
    {
        return $this->redirectToRoute('front');
    }

    protected function login($username, $password , $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user =  $em->getRepository('App:User')->findOneByEmail($username);
        $bool =  false;
        if($user){
            $bool = ($encoder->isPasswordValid($user, $password)) ? true : false;
        }
        return $bool;
    }
}
