<?php

namespace App\Controller;

use App\Entity\Localisation;
use App\Form\LocalisationType;
use App\Repository\LocalisationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/localisation")
 */
class LocalisationController extends Controller
{
    /**
     * @Route("/liste/{id}", name="localisation_index", methods="GET")
     */
    public function index($id = null, LocalisationRepository $localisationRepository): Response
    {
        if($id){
            $parent = $localisationRepository->find($id);
            return $this->render('localisation/index.html.twig', [ 'parent' => $parent, 'type' => 'parent', 'localisations' => $localisationRepository->findBy(['parent' => $parent])]);

        }else{
            return $this->render('localisation/index.html.twig', ['type' => 'null' , 'localisations' => $localisationRepository->findBy(['type' => 'ville'])]);

        }
    }

    /**
     * @Route("/new/{type}/{parent}/{id}", name="localisation_new", methods="GET|POST")
     */
    public function new(Request $request , $type , $parent, $id = null): Response
    {
        $localisation = new Localisation();
        $em = $this->getDoctrine()->getManager();
        if($id != null ){
            $localisation = $em->getRepository(Localisation::class)->find($id);
        }
        if($parent != "null" ){
            $parent = $em->getRepository(Localisation::class)->find($parent);
        }

        $form = $this->createForm(LocalisationType::class, $localisation , array('type' => $type) );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $localisation->setType($type);
            if($parent != "null" && $parent != null) $localisation->setParent($parent);
            $em->persist($localisation);
            $em->flush();

            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('localisation_show' , [ 'id' => $localisation->getIdentifiant()  ] );
        }

        return $this->render('localisation/new.html.twig', [
            'localisation' => $localisation,
            'parent' =>$parent,
            'type' => $type,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="localisation_show", methods="GET")
     */
    public function show(Localisation $localisation): Response
    {
        return $this->render('localisation/show.html.twig', ['localisation' => $localisation]);
    }

    /**
     * @Route("/{id}/edit", name="localisation_edit", methods="GET|POST")
     */
    public function edit(Request $request, Localisation $localisation): Response
    {
        $form = $this->createForm(LocalisationType::class, $localisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('localisation_edit', ['id' => $localisation->getId()]);
        }

        return $this->render('localisation/edit.html.twig', [
            'localisation' => $localisation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="localisation_delete", methods="DELETE")
     */
    public function delete(Request $request, Localisation $localisation): Response
    {
        $parent = $localisation->getParent();
        if ($this->isCsrfTokenValid('delete'.$localisation->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($localisation);
            $em->flush();
        }
        if($parent){
            return $this->redirectToRoute('localisation_index' , ['id' => $parent->getIdentifiant() ] );
        }else{
            return $this->redirectToRoute('localisation_index');
        }

    }
}
