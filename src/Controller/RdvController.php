<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Prestataire;
use App\Entity\Rdv;
use App\Form\RdvType;
use App\Repository\RdvRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/rdv")
 */
class RdvController extends Controller
{
    /**
     * @Route("/", name="rdv_index", methods="GET")
     */
    public function index(RdvRepository $rdvRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $prestataires = $em->getRepository(Prestataire::class)->findAll();
        $clients = $em->getRepository(Client::class)->findAll();

       // dump($prestataires[0]->getAgendas('2018-11-09'));
        //die();

        return $this->render('rdv/index.html.twig', [
            'rdvs' => $rdvRepository->findAll(),
            'prestataires' => $prestataires,
            'clients' => $clients
        ]);
    }

    /**
     * @Route("/validations/{prest}", name="rdv_validations")
     */
    public function validations($prest , Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
            $date = new \DateTime($parametersAsArray['date']);
            $time = explode(':',$parametersAsArray['heure']);
            $date->setTime($time[0], $time[1]);
            $prestataire = $em->getRepository(Prestataire::class)->find($parametersAsArray['prestataire']);
            $client = $em->getRepository(Client::class)->find($parametersAsArray['client']);
            $rdv = $em->getRepository(Rdv::class)->findOneBy( [ 'date' =>  $date , 'prestataire' => $prestataire ] );
            $retour = [];
            $message = $this->container->get('app.sms');
            $tel = $client->getVille()->getPays()->getIndicatif().$client->getTelephone();
            $code = new \App\Entity\Code();
            $code->setEtat(false);
            $code->setClient($client);
            $em->persist($code);
            $em->flush();
            $message->smsSendCodeRdv($tel, $code , $date);
            if($rdv){
                $retour = array( 'code' => 0 , 'message' => 'Le Prestataire n\'est pas disponible a cette preiode' );
            }else{
                $retour = array( 'code' => 1 , 'message' => 'SMS Envoyer' , 'data' => $tel  );
            }
            echo json_encode($retour);
        }
        die();
    }

    /**
     * @Route("/confirmation/{prest}", name="rdv_confirmartion")
     */
    public function confirmation($prest , Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
            $date = new \DateTime($parametersAsArray['date']);
            $time = explode(':',$parametersAsArray['heure']);
            $date->setTime($time[0], $time[1]);
            $prestataire = $em->getRepository(Prestataire::class)->find($parametersAsArray['prestataire']);
            $client = $em->getRepository(Client::class)->find($parametersAsArray['client']);
            $codeRdv = $em->getRepository(\App\Entity\Code::class)->find($parametersAsArray['code']);
            $retour = [];

            $message = $this->container->get('app.sms');

            $tel = $client->getVille()->getPays()->getIndicatif().$client->getTelephone();
            if($codeRdv){
                $rdv = new Rdv();
                $rdv->setDate($date);
                $rdv->setClient($client);
                $rdv->setPrestataire($prestataire);
                $rdv->setCommentaireAvant($parametersAsArray['commentaire']);
                $rdv->setStatus(0);
                $rdv->addCode($codeRdv);
                $em->persist($rdv);
                $em->flush();
                $retour = array( 'code' => 1 , 'message' => 'RDV confirmer'  );
            }else{
                $retour = array( 'code' => 0 , 'message' => 'Le code est invalide' );
            }
            echo json_encode($retour);
        }
        die();
    }

    /**
     * @Route("/new", name="rdv_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $rdv = new Rdv();
        $form = $this->createForm(RdvType::class, $rdv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rdv);
            $em->flush();

            return $this->redirectToRoute('rdv_index');
        }

        return $this->render('rdv/new.html.twig', [
            'rdv' => $rdv,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rdv_show", methods="GET")
     */
    public function show(Rdv $rdv): Response
    {
        return $this->render('rdv/show.html.twig', ['rdv' => $rdv]);
    }

    /**
     * @Route("/{id}/edit", name="rdv_edit", methods="GET|POST")
     */
    public function edit(Request $request, Rdv $rdv): Response
    {
        $form = $this->createForm(RdvType::class, $rdv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rdv_edit', ['id' => $rdv->getId()]);
        }

        return $this->render('rdv/edit.html.twig', [
            'rdv' => $rdv,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rdv_delete", methods="DELETE")
     */
    public function delete(Request $request, Rdv $rdv): Response
    {
        if ($this->isCsrfTokenValid('delete'.$rdv->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rdv);
            $em->flush();
        }

        return $this->redirectToRoute('rdv_index');
    }
}
