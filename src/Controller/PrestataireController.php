<?php

namespace App\Controller;

use App\Entity\Localisation;
use App\Entity\Prestataire;
use App\Form\PrestataireType;
use App\Repository\PrestataireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/prestataire")
 */
class PrestataireController extends Controller
{
    /**
     * @Route("/liste/{id}", name="prestataire_index", methods="GET")
     */
    public function index($id = null, PrestataireRepository $prestataireRepository): Response
    {
        if($id){
            $parent = $prestataireRepository->find($id);
            return $this->render('prestataire/index.html.twig', [ 'parent' => $parent, 'type' => 'parent', 'prestataires' => $prestataireRepository->findBy(['parent' => $parent])]);

        }else{
            return $this->render('prestataire/index.html.twig', ['type' => 'null' , 'prestataires' => $prestataireRepository->findBy(['parent' => null])]);

        }

        //dump($prestataireRepository->findAll()[0]->getNom()); die();
        //return $this->render('prestataire/index.html.twig', ['prestataires' => $prestataireRepository->findAll()]);
    }

    /**
     * @Route("/new/{type}/{parent}/{id}", name="prestataire_new", methods="GET|POST")
     */
    public function new($id= null, Request $request, $type , $parent): Response
    {
        $prestataire = new Prestataire();
        $em = $this->getDoctrine()->getManager();
        if($id != null ){
            $prestataire = $em->getRepository(Prestataire::class)->find($id);
        }
        if($parent != "null" ){
            $parent = $em->getRepository(Prestataire::class)->find($parent);
        }
        $villes = $em->getRepository(Localisation::class)->findBy( ['type' => 'ville'] );
        $form = $this->createForm(PrestataireType::class, $prestataire , [ 'type' => $type ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($request->get('ville')){
                $ville = $em->getRepository(Localisation::class)->find($request->get('ville'));
                if($ville) $prestataire->setVille($ville);
            }
            if($request->get('commune')){
                $commune = $em->getRepository(Localisation::class)->find($request->get('commune'));
                if($commune) $prestataire->setCommune($commune);
            }

            $em = $this->getDoctrine()->getManager();
            foreach ($prestataire->getTarif() as $key => $tarif){
                if($tarif->getPrestataire() ==null) $prestataire->getTarif()[$key]->setPrestataire($prestataire);
            }
            foreach ($prestataire->getAgenda() as $key => $agenda){
                if($agenda->getPrestataire() ==null) $prestataire->getAgenda()[$key]->setPrestataire($prestataire);
            }
            if($parent != "null" && $parent != null) $prestataire->setParent($parent);
            $em->persist($prestataire);
            $em->flush();

            $request->getSession()->getFlashBag()->add('ajout', 'Opération réussie avec succès !');
            return $this->redirectToRoute('prestataire_show' , [ 'id' => $prestataire->getIdentifiant()  ] );
        }

        return $this->render('prestataire/new.html.twig', [
            'prestataire' => $prestataire,
            'type' => $type,
            'villes' => $villes,
            'parent' =>$parent,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/agendas-prestataire/{prest}", name="prestataire_agendas")
     */
    public function agendas( $prest , Request $request): Response
    {
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
            $date = new \DateTime($parametersAsArray['date']);
            $em = $this->getDoctrine()->getManager();
            $prestataire = $em->getRepository(Prestataire::class)->find($prest);
           $agendas =  $prestataire->getHeure($date->format('Y-m-d'));
           echo json_encode($agendas);
        }


        die();
    }

    /**
     * @Route("/{id}", name="prestataire_show", methods="GET")
     */
    public function show(Prestataire $prestataire): Response
    {
        return $this->render('prestataire/show.html.twig', ['prestataire' => $prestataire]);
    }

    /**
     * @Route("/{id}/edit", name="prestataire_edit", methods="GET|POST")
     */
    public function edit(Request $request, Prestataire $prestataire): Response
    {
        $form = $this->createForm(PrestataireType::class, $prestataire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('prestataire_edit', ['id' => $prestataire->getId()]);
        }

        return $this->render('prestataire/edit.html.twig', [
            'prestataire' => $prestataire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="prestataire_delete", methods="DELETE")
     */
    public function delete(Request $request, Prestataire $prestataire): Response
    {
        if ($this->isCsrfTokenValid('delete'.$prestataire->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($prestataire);
            $em->flush();
        }

        return $this->redirectToRoute('prestataire_index');
    }
}
