<?php

namespace App\Form;

use App\Entity\Prestataire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrestataireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('presentation')
            ->add('adresse')
            ->add('email')
            ->add('lienFace')
            ->add('lienTwiter')
            ->add('lienLinkedin')
            ->add('longitude')
            ->add('latitude')
            ->add('autreInfo')
            ->add('moyenTransport')
            ->add('decrispTarif')
            ->add('commentaire')
            ->add('langue')
            ->add('image' , ImageType::class, array( 'required' => false ))
            ->add('etat', ChoiceType::class, array(
                'choices'  => array(
                    'OUI' => 1,
                    'NON' => 0

                ),
            ))
            ->add('tarif', CollectionType::class, array(

                'entry_type'   => TarifType::class,
                'allow_add' => true,
                'allow_delete' => true,

            ))
            ->add('agenda', CollectionType::class, array(

                'entry_type'   => AgendaType::class,
                'allow_add' => true,
                'allow_delete' => true,

            ))
        ;
        if($options['type'] == 'entreprise'){
            $builder
            ->add('entreprise' , EntrepriseType::class )
                ;
        }
        if($options['type'] == 'particulier'){
            $builder
                ->add('particulier' , ParticulierType::class )
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prestataire::class,
            'type' => null
        ]);
    }
}
