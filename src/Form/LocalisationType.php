<?php

namespace App\Form;

use App\Entity\Localisation;
use App\Entity\Pays;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocalisationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle')
            ->add('description')
            ->add('pays', EntityType::class, array(
                'class' => Pays::class,
                'choice_label' => 'nom',
                'multiple' => false,
            ))
        ;
        if($options['type'] == 'commune'){
            $builder
                ->remove('pays')
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Localisation::class,
            'type' => null
        ]);
    }
}
