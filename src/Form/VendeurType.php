<?php

namespace App\Form;

use App\Entity\Vendeur;
use App\Entity\Pays;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class VendeurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('tel')
            ->add('ville')
            ->add('quartier')
            ->add('adresse')
            ->add('description')
            ->add('image' , ImageType::class, array( 'required' => false ))
            ->add('pays', EntityType::class, array(
                'class' => Pays::class,
                'choice_label' => 'nom',
                // 'multiple' => true,
                // 'expanded' => true,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vendeur::class,
        ]);
    }
}
